import asyncio
import logging
import sys
import re

from aiohttp import ClientSession
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString
from urllib.request import urlopen, Request
from typing import Dict, List

from core.kodik import KodikFile, KodikLoader

import click

logging.basicConfig(level=logging.DEBUG)

HEADERS = {
    "Host": "yummyanime.club",
    "Referer": "https://yummyanime.club/",
    "DNT": '1',
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0",
}

# Example URL
URL = "https://yummyanime.club/catalog/item/epoxa-smyt-2"

async def get_video_blocks_info(soup: BeautifulSoup):
    video_block_info = {}
    video_blocks = soup.findAll('div', 'video-block')
    for block in video_blocks:
        description = block.find('div', {'class': 'video-block-description'}).get_text(strip=True)
        episodes_container = block.find('div', {'class':'episodes-container'}).findAll('div', {'class': 'video-button'})
        episodes_links = []
        for episode in episodes_container:
            episodes_links.append(episode.get('data-href'))

        voice, player = re.findall(r'Озвучка\s(\w+).\sПлеер\s(\w+)', description)[0]
        video_block_info[voice] = {'player':player, 'links':episodes_links}    

    return video_block_info

async def get_main_info(soup: BeautifulSoup) -> (str, Dict):          
    """ This function parses the title and information about the anime 
        Not perfect, but it works :) """
    title = soup.find_next('h1').get_text(strip=True)
    info = {}

    main_info_ul = soup.find('ul', {'class': 'content-main-info'})
    for li in main_info_ul.findAll('li'):
        span = li.find('span')
        if span:
            if span.string == 'Просмотров:':
                span.decompose()
                info['views'] = li.get_text(strip=True)
            if span.string == 'Жанр:':
                genres = []
                for li in li.find('ul').findAll('li', recursive=False):
                    genres.append(li.get_text(strip=True))

                    info['genre'] = genres

            if span.string == 'Тип:':
                span.decompose()
                info['type'] = li.get_text(strip=True)

            if span.string == 'Серии:':
                span.decompose()
                info['count'] = li.get_text(strip=True)           
            if span.string == 'Озвучка:':
                voices = []
                span.decompose()
                for li2 in li.find('ul', {'class':'animeVoices'}).findAll('li', recursive=False):
                    if isinstance(li2, Tag):
                        try:
                            t = li2.find('a', {'class':'studio-name'}).get_text(strip=True)
                        except AttributeError:
                            t = li2.get_text(strip=True)
                        
                        voices.append(t)
                   
                    if isinstance(li2, NavigableString):
                        voices.append(li2.strip())

                info['voices'] = voices

    return title, info

async def get_html(url: str, session: ClientSession) -> str:
    async with session.get(url, headers=HEADERS) as r:
        html = await r.content.read()
    return html.decode()

async def prepare_videos(loader: KodikLoader, videofiles: List[KodikFile]):
    for videofile in videofiles:
        await loader.prepare(videofile)

async def proceed_videos(loader: KodikLoader, videofiles: List[KodikFile]):
    for video in videofiles:
        while not video.prepared:
            await asyncio.sleep(.5)
        await loader.download(video)

async def main():
    session = ClientSession()
    html = await get_html(URL, session)
    soup = BeautifulSoup(html, 'lxml')
    content = soup.find('div', {'class':['content-page', 'anime-page']})
    title, main_info = await get_main_info(content)
    video_blocks = await get_video_blocks_info(content)

    video_files = []
    for link in video_blocks['AniDUB']['links'][:1]:
        f = KodikFile(title, '360')
        f.page_url = link
        video_files.append(f)

    loader = KodikLoader(session=session)

    await prepare_videos(loader, video_files)
    await proceed_videos(loader, video_files[:1])

    await session.close()


if __name__ == "__main__":
    loop =  asyncio.get_event_loop()
    loop.run_until_complete(main())
