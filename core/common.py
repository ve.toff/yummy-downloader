import asyncio
import aiohttp
import logging
import os


logging.basicConfig(level=logging.DEBUG)


class VideoFile:
    def __init__(self):
        self.page_url = None
        self.file_url = None
        self.title = None
        self.size = None
        self.prepared = False

    def __repr__(self):
        return f'{self.title}:Prepared:{self.prepared}:{self.file_url}'

    @property
    def filename(self):
        return self.title + self.ext

    @property
    def extension(self):
        raise NotImplemented


class Loader:

    MAX_CHUNK_SIZE = 256 * 1024
    HANDLERS = 4
    TIMEOUT = 20 * 60

    def __init__(self, file_path='.', session=None):
        self._session = session or aiohttp.ClientSession()
        self._file_path = os.path.abspath(file_path)

    def file_sink(self, path):
        with open(path, 'r+b') as f:
            while True:
                chunk = yield
                if not chunk:
                    break

                f.seek(chunk[0], os.SEEK_SET)
                f.write(chunk[1])

    def create_file(self, filename, size):
        path = os.path.join(self._file_path, filename)

        try:
            with open(path, 'w+b') as f:
                f.seek(size-1)
                f.write(b'\0')
        except MemoryError as e:
            logging.error(e)

    async def prepare(self, videofile: VideoFile):
        raise NotImplemented

    async def _download_part(self, url, start, end, fsink):
        i = start
        async with self._session.get(url, headers={'Range': f'bytes={start}-{end}'},
                                     timeout=self.TIMEOUT) as r:
            while True:
                chunk = await r.content.read(self.MAX_CHUNK_SIZE)
                if not chunk:
                    break

                fsink.send((i, chunk))
                i += len(chunk)

        return i - start

    async def proceed_video(self, videofile: VideoFile):
        download_futures = []

        fsink = self.file_sink(os.path.join(
            self._file_path, videofile.filename))
        next(fsink)

        part_size = videofile.size // self.HANDLERS

        for i in range(self.HANDLERS):
            start = i * part_size
            end = min(videofile.size, (i+1) * part_size - 1)

            download_futures.append(self._download_part(
                videofile.file_url, start, end, fsink=fsink))

        for download_future in asyncio.as_completed(download_futures):
            await download_future

        return True

    async def download(self, videofile: VideoFile):
        try:
            self.create_file(videofile.filename, videofile.size)
            pass
        except MemoryError as e:
            logging.error(e)
        else:
            await self.proceed_video(videofile)
